#!/usr/bin/env python

# IMPORTS
import psutil
import argparse
import sys
import numpy as np

# INPUT PARSING

parser = argparse.ArgumentParser(description='A python script suspending/resuming given UNIX\
	processes. This is achived by the psutil package.\n\
	\
	Last modified: 2015.12.06 by Onur Sercinoglu\
	Authors: Onur Sercinoglu')

parser.add_argument('--PID',type=int,nargs='+',default=[False],help='Process ID number(s) of\
	process(es) for which a hold is requested')

parser.add_argument('--pname',type=str,nargs=1,default=[False],help='A pattern text according to\
	which requested processes will be found. Only used when PID is not provided.')

parser.add_argument('--PIDfile',type=str,nargs=1,default=[False],help='A text file containing the\
	Process ID number(s) of process(es) for which a hold is requested in CSV format')

parser.add_argument('--mode',type=str,nargs=1,choices=['suspend','resume'],help='Specifies the\
	mode of call: suspend or resume')

parser.add_argument('--outPIDfile',type=str,nargs=1,default=[False],help='Name of the text file\
	into which PID numbers which are hold by the program are written in CSV format')

args = parser.parse_args()
print args.pname[0]
# Get outPIDfile according to the input argument
if args.outPIDfile[0] != False:
	outPIDfile = args.outPIDfile[0]
elif args.outPIDfile[0] == False:
	outPIDfile = 'batchHold.dat'

# Get process ID numbers according to the input arguments
PID = list()

if args.PID[0] == False:
	if args.pname[0] != False:
		for proc in psutil.process_iter():
			if proc.name() == args.pname[0]:
				PID.append(proc.pid)

	else:
		print 'Error: You must specify either --PID or --pname.'

else:
	PID = args.PID

# Get PID numbers from PIDfile (if provided).
if args.PIDfile[0]:
	f = open(args.PIDfile[0],'r')
	PIDfile_PID = f.readlines()[0]
	PIDfile_PID = PIDfile_PID.split(',')
	PIDfile_PID = map(int,PIDfile_PID)

	# merge these numbers with the PID list
	PID = PID + PIDfile_PID

	# Uniquify this list
	PID = np.unique(PID)
	print PID

# Check all processes in the PID list for existence. If even on PID does not exist, abort.
for PIDno in PID:
	if not psutil.pid_exists(PIDno):
		sys.exit('PID %i does not exist! Please remove it from your input arguments and rerun this script. Aborting now.' % PIDno)

# Hold all processes in the PID list.
for PIDno in PID:
	proc = psutil.Process(PIDno)

	if args.mode[0] == 'suspend':
		proc.suspend()
		print 'Suspending PID %i' % PIDno
	elif args.mode[0] == 'resume':
		proc.resume()
		print 'Resuming PID %i' % PIDno

# Write into outPIDfile
outPIDfile_line = ",".join(map(str,PID))
f = open(outPIDfile,'w')
f.write(outPIDfile_line)
f.close()
